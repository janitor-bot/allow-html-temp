(async () => {

  await optionsMigrate();
  await optionsInit();
  await messenger.storage.onChanged.addListener(optionsInit);

  // Add eventListener for onClicked on the toolbar button
  messenger.browserAction.onClicked.addListener(onToolbarButtonClicked);
  async function onToolbarButtonClicked(tab, info) {
    consoleDebug("AHT: browserAction.onClicked: Toolbar button fired");
    consoleDebug("AHT: browserAction.onClicked: tab.id = " + tab.id);
    consoleDebug("AHT: browserAction.onClicked: info.modifiers = " + info.modifiers);

    ahtFunctions.allowHtmlTemp(tab.id, info.modifiers, false);
  }

  // Add eventListener for onClicked on the header button
  messenger.messageDisplayAction.onClicked.addListener(onHeaderButtonClicked);
  async function onHeaderButtonClicked(tab, info) {
    consoleDebug("AHT: messageDisplayAction.onClicked: Header button fired");
    consoleDebug("AHT: messageDisplayAction.onClicked: tab.id = " + tab.id);
    consoleDebug("AHT: messageDisplayAction.onClicked: info.modifiers = " + info.modifiers);

    ahtFunctions.allowHtmlTemp(tab.id, info.modifiers, false);
  }

  // Add eventListener for remote item in doorhanger context menu
  messenger.allowHtmlTemp.onClick.addListener((tabId) => {
    consoleDebug("AHT: allowHtmlTemp.onClick.addListener: tabId = " + tabId);
    ahtFunctions.allowHtmlTemp(tabId, "", true);
  });

})();